```
snakemake \
	--rulegraph \
	--configfile pipeline_config.yaml \
	--cores 1 \
	all_possible_multiqc \
	> rulegraph
	
cat rulegraph | dot -Tsvg > rulegraph_raw.svg
```
