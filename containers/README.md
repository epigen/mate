Singularity v3.8.6 was used to pull all the containers from [BioContainers](https://biocontainers.pro/) as well as to build the custom made containers.

The commands used to pull the containers:

```
#fastq-screen
singularity pull docker://quay.io/biocontainers/fastq-screen_0.14.0--pl5321hdfd78af_2

#trimmomatic
singularity pull docker://quay.io/biocontainers/trimmomatic:0.39--1

#kraken2
singularity pull docker://quay.io/biocontainers/kraken2_2.1.2--pl5321h7d875b9_1

#bracken
singularity pull docker://quay.io/biocontainers/bracken_2.6.1--py39h7cff6ad_2

#krona
singularity pull docker://quay.io/biocontainers/krona_2.7.1--pl526_5

#metaphlan
SINGULARITY_TMPDIR="/absolute_path/tmp_metaphlan"

sudo -E singularity build --tmpdir $SINGULARITY_TMPDIR \
	metaphlan4.sif \
	metaphlan4.def \
	> metaphlan4.build.log

#hclust2	
singularity pull docker://quay.io/biocontainers/hclust2:1.0.0--py_0
```
