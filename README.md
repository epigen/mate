# MATE

MetATranscriptomE analysing pipeline.


## Table of Contents
1. [Overview](#overview)
2. [Pipeline Setup](#pipeline-setup)
3. [Pipeline Usage](#pipeline-usage)
4. [Folder Structure And Contents Of The Results](#folder-structure-and-contents-of-the-results)
5. [Limitations](#limitations)
6. [Questions, Comments, Suggestions](#questions-comments-suggestions)
7. [ToDo](#todo)





## Overview



All the used bioinformatics tools are wrapped in singularity containers. For more details about the containers' specifics, see [containers/README.md](https://gitlab.com/epigen/mate/-/blob/main/containers/README.md).

In the following figure, all the `snakemake` rules (code components) of the pipeline are shown as colored bubbles. The rules are organised in such a way that the high-level rules - the rules which are called when running the pipeline - are shown at the bottom of the figure. The code components are executed in groups, when a high-level rule is called, all the rules of the same color will be run. When there is an arrow between two code components, results of the source component have to be available before the destination code component is executed. It means that all the code components to the left from a called high-level rule have to be done before the rule starts to be executed - this can happen in two ways, either by running the high-level rules consecutively from left to right and inspecting intermediate results of the pipeline or by executing any high-level rule in which case all the missing bits of the pipeline will be run automaticaly, regardless of the colors. The arrows starting in the `check_presence` are dashed for better readability. When a conventional bioinformatic tool is used in a snakemake rule without mentioning the tool in the rule name, the tool is written below the rule bubble.

![Snakemake rulegraph](imgs/rulegraph.png "Snakemake rulegraph")


### Dependencies

The pipeline uses `snakemake` for workflow management and thus also depends on `python3`. It needs `singularity` to be installed on the host system as well. The versions used in our TSD setup are listed here:


<center>

| Software | Used Version |
| :--- | :---: |
| `python3` | 3.9.5 |
| `singularity` | 3.7.3 |
| `snakemake` | 6.6.1 |

</center>


## Pipeline Setup

### Initialise dependencies in TSD before executing the pipeline 

Before executing the pipeline on the submit node, run the following commands to make all the necessary software tools available in the current terminal window:

```
module purge
module load Python/3.9.5-GCCcore-10.3.0
module load singularity/3.7.3
module load snakemake/6.6.1-foss-2021a
```

### General setup

1. Clone/download/transfer content of a tagged version of this repository to the computing infrastructure of your choice.
2. Download and collect all the necessary data resources: host reference genome, `kraken2` database and `krona` taxonomy file.
3. Make a copy of `pipeline_config_template.yaml`, call it for example `pipeline_config.yaml`.
3. Create a file containing info about all the files included in the analysis and in the next step set the file's absolute path to be the value of the `INPUT_FASTQ_INFO_SHEET` variable in the `pipeline_config.yaml` file. Check `file_info_example.tsv` for an example of such a file. The file have to contain tab-separated values with the first line of the file being its header and consisting of the following column names: `file_name`, `sample_name`, `file_id`, `lane`, `read`, `group`, `instrument_id`, and `flowcell_id` where the columns' content is: 

      - `file_path` is absolute path to a file,
      - `sample_name` is the prefix of the file name preceding `_lane_read_001.fastq.gz`,
      - `file_id` is the prefix of the file name preceding `.fastq.gz`,
      - `lane` is lane ID starting with letter L followed by three digits,
      - `read` is read ID, either R1 or R2,
      - `group` specifies groups according to which the read counts of different samples will be separated into the merged read count files (for example, different tissue types),
      - `instrument_id` is the ID of the sequencing instrument on which the sample was sequenced - can be obtained from the `fastq` file,
      - `flowcell_id` is the ID of the flowcell on which the sample was sequenced - can be obtained from the `fastq` file.

	There is a helping script called `generate_file_info.sh` in the `scripts` subfolder which is meant to help to create such `file_info.tsv` file. The script iterates through all the `fastq.gz` files or links that are specified in the input pattern of the script and generates the `file_info.tsv` file based on the file names and content of the files. Usage of the script follows:

	```
	bash scripts/generate_file_info.sh <FILE_LISTING_PATTERN> \
		> file_info.tsv \
		2> generate_file_info.log
	```

4. Fill all the remaining information specific for the project at hand into the `pipeline_config.yaml` file.
5. If slurm is the server's workload manager, `slurm_config.yaml` can be used to specify job requirements (this is true for TSD). Adjust the account id in the `__default__` rule to correspond to the used account. In the commands of the following section, slurm is assumed to be the workload manager.



## Pipeline Usage


1. Initialisation step:

	```
	# dry run: run it to see which commands will be executed without executing them
	snakemake -np --configfile pipeline_config.yaml --cores 1 -- init
	
	
	# running init step for real
	NOW=$(date +%Y-%m-%d_%H%M%S)	
	snakemake -p --configfile pipeline_config.yaml --cores 1 \
	      -- init \
		>>  snakemake_init_${NOW}.stdout \
		2>> snakemake_init_${NOW}.stderr
	```

      Output data of this step will be stored in the subfolder called `00_presence`. It takes only a couple of seconds to run this step, no data analysis is done yet, but the files necessary for the further processing are generated. Proceed to the next step of the analysis only if the files `00_params.done` and `00_init.done` are present in the `ANALYSIS_FOLDER`. Otherwise, check the content of the generated `snakemake_init*` files to narrow down the existing issue, fix it and re-run this step of the analysis.


2. Remove host reads:

	```
	# dry run: run it to see which commands will be executed without executing them
	snakemake -np --configfile pipeline_config.yaml --cores 1 remove_host_reads

	NOW=$(date +%Y-%m-%d_%H%M%S)
	snakemake -p --verbose \
		--reason \
		--max-status-checks-per-second 0.01 \
		--cores 10 \
		--jobs  10 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} --cpus-per-task {cluster.cpus-per-task} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --job-name {cluster.job-name} --output {cluster.output} --error {cluster.error}" \
		-- remove_host_reads \
		>>  snakemake_remove_host_reads_${NOW}.stdout \
		2>> snakemake_remove_host_reads_${NOW}.stderr &
	```


3. Kraken:

	```
	# dry run: run it to see which commands will be executed without executing them
	snakemake -np --configfile pipeline_config.yaml --cores 1 kraken

	NOW=$(date +%Y-%m-%d_%H%M%S)
	snakemake -p --verbose \
		--reason \
		--max-status-checks-per-second 0.01 \
		--cores 10 \
		--jobs  10 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} --cpus-per-task {cluster.cpus-per-task} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --job-name {cluster.job-name} --output {cluster.output} --error {cluster.error}" \
		-- kraken \
		>>  snakemake_kraken_${NOW}.stdout \
		2>> snakemake_kraken_${NOW}.stderr &
	```


4. Krona:

	```
	# dry run: run it to see which commands will be executed without executing them
	snakemake -np --configfile pipeline_config.yaml --cores 1 krona


	NOW=$(date +%Y-%m-%d_%H%M%S)
	snakemake -p --verbose \
		--reason \
		--max-status-checks-per-second 0.01 \
		--cores 1 \
		--jobs 20 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} --cpus-per-task {cluster.cpus-per-task} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --job-name {cluster.job-name} --output {cluster.output} --error {cluster.error}" \
		-- krona \
                >>  snakemake_krona_${NOW}.stdout \
                2>> snakemake_krona_${NOW}.stderr &
	```

5. Metaphlan:


	```
	# dry run: run it to see which commands will be executed without executing them
	snakemake -np --configfile pipeline_config.yaml --cores 1 metaphlan

	NOW=$(date +%Y-%m-%d_%H%M%S)
	snakemake -p --verbose \
		--reason \
		--max-status-checks-per-second 0.01 \
		--cores 10 \
		--jobs  10 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} --cpus-per-task {cluster.cpus-per-task} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --job-name {cluster.job-name} --output {cluster.output} --error {cluster.error}" \
		-- metaphlan \
		>>  snakemake_metaphlan_${NOW}.stdout \
		2>> snakemake_metaphlan_${NOW}.stderr &
	```


## Folder Structure And Contents Of The Results

Files and folders will gradually keep appearing after each processing step. Here is a listing of the main files and folders divided into smaller chunks depending on the step of the analysis, in which the files/folders are generated.

1. After a successful run of the initialisation step of the pipeline, the content of the `ANALYSIS_FOLDER` will be:

```
	.
	├── 00_presence
	│   ├── 01_files
	│   ├── 02_sample_lanes
	│   └── 03_samples
	├── 00_init.done
	├── 00_params.done
	└── params.sh
```

The file `00_params.done` indicates that the `params.sh` file was created successfully and the file `00_init.done` indicates that the `00_presence` contains everything necessary for the further analysis steps.


2. After a successful run of the host reads removal step of the pipeline, the following files and folders will be added to the `ANALYSIS_FOLDER`:

```
	├── 01_cleaned_non_host_reads
	│   ├── cleaned_fastq
	│   ├── fastq_screen
	│   ├── host_genome_index
	│   └── lanes_merged_fastq
	├── 01_params_remove_host_reads.done
	├── 01_remove_host_reads.done
	└── params.sh
```

3. After a successful run of the kraken taxonomic step of the pipeline, the following files and folders will be added to the `ANALYSIS_FOLDER`:

```
	├── 02_kraken2
	│   ├── bracken_lanes_merged
	│   ├── bracken_lanes_separately
	│   ├── kraken_lanes_merged
	│   └── kraken_lanes_separately
	├── 02_kraken2.done
	├── 02_params_kraken2.done
	└── params.sh
```

4. After a successful run of the krona visualising step of the pipeline, the following files and folders will be added to the `ANALYSIS_FOLDER`:

```
	├── 03_krona
	│   ├── b_groups_together
	│   ├── b_lanes_merged
	│   ├── b_lanes_together
	│   ├── k2_groups_together
	│   ├── k2_lanes_merged
	│   └── k2_lanes_together
	├── 03_krona.done
	├── 03_params_krona.done
	└── params.sh
```

5. After a successful run of the metaphlan taxonomic step of the pipeline, the following files and folders will be added to the `ANALYSIS_FOLDER`:

```
	├── 04_metaphlan
	│   ├── lanes_merged
	│   │   ├── bowtie2out
	│   │   ├── diversities
	│   │   ├── heatmap
	│   │   ├── merged_abubndances
	│   │   └── post_mapping
	│   └── lanes_separately
	│       ├── bowtie2out
	│       ├── diversities
	│       ├── heatmap
	│       ├── merged_abubndances
	│       └── post_mapping
	├── 04_params_metaphlan.done
	├── 04_metaphlan.done
	└── params.sh
```




## Limitations

At the moment, the pipeline can process Illumina pair-end sequenced reads that are belong to the portion of RNAseq data which were not mapping to the host reference genome with STAR.

## Questions, Comments, Suggestions

Please raise an issue in the repository.

## ToDo

1. Expand this README.md
2. Add de novo assembly to explore 


