#!/bin/bash

set -e 

PARAMS_FILE="$1"
FORWARD_READS_GZ="$2"
REVERSE_READS_GZ="$3"
SAMPLE="$4"
STRING="$5"
THREADS="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tFORWARD_READS_GZ=\"${FORWARD_READS_GZ}\""
echo -e "\tREVERSE_READS_GZ=\"${REVERSE_READS_GZ}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/metaphlan_${STRING}_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/metaphlan_${STRING}_${SAMPLE}_WD"
mkdir -p "${WDIR}"

FORWARD_READS_FILENAME=$(basename ${FORWARD_READS_GZ})
REVERSE_READS_FILENAME=$(basename ${REVERSE_READS_GZ})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tFORWARD_READS_FILENAME=\"${FORWARD_READS_FILENAME}\""
echo -e "\tREVERSE_READS_FILENAME=\"${REVERSE_READS_FILENAME}\""
echo



if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp    ${FORWARD_READS_GZ} ${SCRATCH}
cp    ${REVERSE_READS_GZ} ${SCRATCH}

ls -la ${SCRATCH}
echo


echo "print metaphlan help:"
echo
# print out metaphlan help
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan -h \
		>  ${LOG_SINGULARITY} \
		2> ${LOG_SINGULARITY}


echo "read count forward:" >> ${LOG_SINGULARITY}
echo >> ${LOG_SINGULARITY}
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		read_fastx.py ${FOLDER_IN_CONTAINER}/${FORWARD_READS_FILENAME} \
		> /dev/null \
		2>> ${LOG_SINGULARITY}

echo "read count reverse:" >> ${LOG_SINGULARITY}
echo >> ${LOG_SINGULARITY}
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		read_fastx.py ${FOLDER_IN_CONTAINER}/${REVERSE_READS_FILENAME} \
		> /dev/null \
		2>> ${LOG_SINGULARITY}


echo "run metaphlan4 analysis, including unclassified estimate output:"
echo
# run metaphlan
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${FORWARD_READS_FILENAME},${FOLDER_IN_CONTAINER}/${REVERSE_READS_FILENAME} \
			--input_type fastq \
			--nproc ${THREADS} \
			--bowtie2out ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}.bowtie2.bz2 \
			--unclassified_estimation \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_unclassified_est.txt \
			--samout ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}.sam \
			--offline \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}





ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm    ${SCRATCH}/${FORWARD_READS_FILENAME}
rm    ${SCRATCH}/${REVERSE_READS_FILENAME}

mv ${SCRATCH}/mpa4_metagenome_${STRING}_${SAMPLE}.bowtie2.bz2      ${OUT_FOLDER}
mv ${SCRATCH}/mpa4_metagenome_${STRING}_${SAMPLE}_unclassified_est.txt ${OUT_FOLDER}
mv ${SCRATCH}/mpa4_metagenome_${STRING}_${SAMPLE}.sam              ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
