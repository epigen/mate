#!/bin/bash

FILE_LISTING_PATTERN="$1"

# echo "${FILE_LISTING_PATTERN}"


#print file header
echo -e "file_path\tsample_name\tfile_id\tlane\tread\tgroup\tinstrument_id\tflowcell_id"


FILE_PATHS_STRING="`eval "${FILE_LISTING_PATTERN}"`"

# echo -e "${FILE_PATHS_STRING}"

readarray -t FILE_PATHS <<< "${FILE_PATHS_STRING}"



for FILE_PATH in "${FILE_PATHS[@]}"
do

	OUT_LINE=""

	FILE_NAME=$(basename "${FILE_PATH}")

	SAMPLE_NAME=$( echo "${FILE_NAME}" | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $1"_"$2}' )
	FILE_ID=$(     echo "${FILE_NAME}" | cut -d "." -f 1-3 )
	LANE=$(        echo "${FILE_NAME}" | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $3}')
	READ_GROUP=$(  echo "${FILE_NAME}" | cut -d "." -f 3 )
        GROUP="NA"
	INSTRUMENT_ID=$(zcat "${FILE_PATH}" | head -1 | awk 'BEGIN{FS=":"}{print substr($1,2)}')
	FLOWCELL_ID=$(  zcat "${FILE_PATH}" | head -1 | awk 'BEGIN{FS=":"}{print $3}' )

	OUT_LINE+="${FILE_PATH}\t"
	OUT_LINE+="${SAMPLE_NAME}\t"
	OUT_LINE+="${FILE_ID}\t"
	OUT_LINE+="${LANE}\t"
	OUT_LINE+="${READ_GROUP}\t"
	OUT_LINE+="${GROUP}\t"
	OUT_LINE+="${INSTRUMENT_ID}\t"
	OUT_LINE+="${FLOWCELL_ID}"


	echo -e "${OUT_LINE}"
done
