#!/bin/bash

set -e 

PARAMS_FILE="$1"
BOWTIE2OUT="$2"
SAMPLE="$3"
STRING="$4"
THREADS="$5"
NREADS="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tBOWTIE2OUT=\"${BOWTIE2OUT}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tNREADS=\"${NREADS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/metaphlan_${STRING}_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/metaphlan_${STRING}_${SAMPLE}_WD"
mkdir -p "${WDIR}"

BOWTIE2OUT_FILENAME=$(basename ${BOWTIE2OUT})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tFORWARD_READS_FILENAME=\"${BOWTIE2OUT_FILENAME}\""
echo



if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp    ${BOWTIE2OUT} ${SCRATCH}

ls -la ${SCRATCH}
echo

echo "run metaphlan4 post mapping analyses:"
echo

# run metaphlan, unclassified estimation
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${BOWTIE2OUT_FILENAME} \
			--input_type bowtie2out \
			--nproc ${THREADS} \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_profile.txt \
			--offline \
			${METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}
		


# run metaphlan, profile
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${BOWTIE2OUT_FILENAME} \
			--input_type bowtie2out \
			--nproc ${THREADS} \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_profile.txt \
			--offline \
			${METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}


# run metaphlan, profile with read stats
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${BOWTIE2OUT_FILENAME} \
			--input_type bowtie2out \
			--nproc ${THREADS} \
			-t rel_ab_w_read_stats \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_profile_w_read_stats.txt \
			--offline \
			${METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}


# run metaphlan, marker abundance table
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${BOWTIE2OUT_FILENAME} \
			--input_type bowtie2out \
			--nproc ${THREADS} \
			-t marker_ab_table \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_marker_ab_table.txt \
			--offline \
			--nreads ${NREADS} \
			${METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}


# run metaphlan, marker presence table
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		metaphlan \
			${FOLDER_IN_CONTAINER}/${BOWTIE2OUT_FILENAME} \
			--input_type bowtie2out \
			--nproc ${THREADS} \
			-t marker_pres_table \
			--output ${FOLDER_IN_CONTAINER}/mpa4_metagenome_${STRING}_${SAMPLE}_marker_presence_table.txt \
			--offline \
			${METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}


ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm    ${SCRATCH}/${BOWTIE2OUT_FILENAME}

mv ${SCRATCH}/mpa4_metagenome_${STRING}_${SAMPLE}* ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
