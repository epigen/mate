#!/bin/bash

set -e 

PARAMS_FILE="$1"
INPUT_KRAKEN2_FILE="$2"
SAMPLE="$3"
STRING="$4"
OUT_FOLDER="$5"
LOG_FOLDER="$6"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tINPUT_KRAKEN2_FILE=\"${INPUT_KRAKEN2_FILE}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo


source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/bracken_${STRING}_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/bracken_${STRING}_${SAMPLE}_WD"
mkdir -p "${WDIR}"

INPUT_KRAKEN2_FILENAME=$(basename ${INPUT_KRAKEN2_FILE})
LAST_KRAKEN2_DB=$(basename ${KRAKEN2_DB})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tINPUT_KRAKEN2_FILENAME=\"${INPUT_KRAKEN2_FILENAME}\""
echo -e "\tLAST_KRAKEN2_DB=\"${LAST_KRAKEN2_DB}\""
echo



if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp -r ${KRAKEN2_DB}         ${SCRATCH}
cp    ${INPUT_KRAKEN2_FILE} ${SCRATCH}

ls -la ${SCRATCH}
echo



# run bracken
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_BRACKEN} \
		bracken \
			${BRACKEN_ADDITIONAL_OPTIONS} \
			-d ${FOLDER_IN_CONTAINER}/${LAST_KRAKEN2_DB} \
			-i ${FOLDER_IN_CONTAINER}/${INPUT_KRAKEN2_FILENAME} \
			-o ${FOLDER_IN_CONTAINER}/b_${STRING}_${SAMPLE}_output.txt \
			-w ${FOLDER_IN_CONTAINER}/b_${STRING}_${SAMPLE}_report.txt \
			> ${LOG_SINGULARITY} \
			2>&1



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm -r ${SCRATCH}/${LAST_KRAKEN2_DB}
rm    ${SCRATCH}/${INPUT_KRAKEN2_FILENAME}

mv    ${SCRATCH}/b_${STRING}_${SAMPLE}_output.txt ${OUT_FOLDER}
mv    ${SCRATCH}/b_${STRING}_${SAMPLE}_report.txt ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi

