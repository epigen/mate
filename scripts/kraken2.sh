#!/bin/bash

set -e 

PARAMS_FILE="$1"
FORWARD_READS_GZ="$2"
REVERSE_READS_GZ="$3"
SAMPLE="$4"
STRING="$5"
THREADS="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tFORWARD_READS_GZ=\"${FORWARD_READS_GZ}\""
echo -e "\tREVERSE_READS_GZ=\"${REVERSE_READS_GZ}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/kraken2_${STRING}_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/kraken2_${STRING}_${SAMPLE}_WD"
mkdir -p "${WDIR}"

FORWARD_READS_FILENAME=$(basename ${FORWARD_READS_GZ})
REVERSE_READS_FILENAME=$(basename ${REVERSE_READS_GZ})

LAST_KRAKEN2_DB=$(basename ${KRAKEN2_DB})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tFORWARD_READS_FILENAME=\"${FORWARD_READS_FILENAME}\""
echo -e "\tREVERSE_READS_FILENAME=\"${REVERSE_READS_FILENAME}\""
echo -e "\tLAST_KRAKEN2_DB=\"${LAST_KRAKEN2_DB}\""
echo



if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp -r ${KRAKEN2_DB}       ${SCRATCH}
cp    ${FORWARD_READS_GZ} ${SCRATCH}
cp    ${REVERSE_READS_GZ} ${SCRATCH}

ls -la ${SCRATCH}
echo



# run kraken2, report zero counts
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_KRAKEN2} \
		kraken2 \
			--db ${FOLDER_IN_CONTAINER}/${LAST_KRAKEN2_DB} \
			--threads ${THREADS} \
			--output ${FOLDER_IN_CONTAINER}/k2_${STRING}_${SAMPLE}_output.txt \
			--report ${FOLDER_IN_CONTAINER}/k2_${STRING}_${SAMPLE}_report.zero_counts.txt \
			--report-zero-counts \
			${KRAKEN2_ADDITIONAL_OPTIONS} \
			--paired ${FOLDER_IN_CONTAINER}/${FORWARD_READS_FILENAME} ${FOLDER_IN_CONTAINER}/${REVERSE_READS_FILENAME} \
			--classified-out   ${FOLDER_IN_CONTAINER}/k2_classified_${STRING}_${SAMPLE}#.fq \
			--unclassified-out ${FOLDER_IN_CONTAINER}/k2_unclassified_${STRING}_${SAMPLE}#.fq \
			> ${LOG_SINGULARITY} \
			2>&1

sort -n -k5 \
	${SCRATCH}/k2_${STRING}_${SAMPLE}_report.zero_counts.txt \
	> ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.zero_counts.sorted.txt



# run kraken2 again to generate report without zero counts
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_KRAKEN2} \
		kraken2 \
			--db ${FOLDER_IN_CONTAINER}/${LAST_KRAKEN2_DB} \
			--threads ${THREADS} \
			--output ${FOLDER_IN_CONTAINER}/k2_${STRING}_${SAMPLE}_output.txt \
			--report ${FOLDER_IN_CONTAINER}/k2_${STRING}_${SAMPLE}_report.txt \
			${KRAKEN2_ADDITIONAL_OPTIONS} \
			--paired ${FOLDER_IN_CONTAINER}/${FORWARD_READS_FILENAME} ${FOLDER_IN_CONTAINER}/${REVERSE_READS_FILENAME} \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}

sort -n -k5 \
	${SCRATCH}/k2_${STRING}_${SAMPLE}_report.txt \
	> ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.sorted.txt



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm -r ${SCRATCH}/${LAST_KRAKEN2_DB} 
rm    ${SCRATCH}/${FORWARD_READS_FILENAME}
rm    ${SCRATCH}/${REVERSE_READS_FILENAME}

mv ${SCRATCH}/k2_${STRING}_${SAMPLE}_output.txt                    ${OUT_FOLDER}
mv ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.txt                    ${OUT_FOLDER}
mv ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.sorted.txt             ${OUT_FOLDER}
mv ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.zero_counts.txt        ${OUT_FOLDER}
mv ${SCRATCH}/k2_${STRING}_${SAMPLE}_report.zero_counts.sorted.txt ${OUT_FOLDER}
mv ${SCRATCH}/k2_classified_${STRING}_${SAMPLE}*.fq                ${OUT_FOLDER}
mv ${SCRATCH}/k2_unclassified_${STRING}_${SAMPLE}*.fq              ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
