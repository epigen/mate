#!/bin/bash

set -e 

PARAMS_FILE="${1}"
SAMPLE="${2}"
INPUT_FASTQ="${3}"
OUT_FOLDER="${4}"
LOG_FOLDER="${5}"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tINPUT_FASTQ=\"${INPUT_FASTQ}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

INPUT_FASTQ_FILENAMES=""

for f in ${INPUT_FASTQ}
do
	INPUT_FASTQ_FILENAMES="${INPUT_FASTQ_FILENAMES} $(basename ${f})"
done


LOG_SINGULARITY="${LOG_FOLDER}/merge_lanes_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/merge_lanes_${SAMPLE}_WD"
mkdir -p "${WDIR}"

echo -e "DERIVED PARAMS:"
echo -e "\tINPUT_FASTQ_FILENAMES=\"${INPUT_FASTQ_FILENAMES}\""
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""


if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

INPUT_FASTQ_IN_SCRATCH=""

for f in ${INPUT_FASTQ_FILENAMES}
do
	INPUT_FASTQ_IN_SCRATCH="${INPUT_FASTQ_IN_SCRATCH} ${SCRATCH}/${f}"
done

echo -e "\tINPUT_FASTQ_IN_SCRATCH=\"${INPUT_FASTQ_IN_SCRATCH}\""
echo



# copy to scratch all the data that are needed for the job

cp ${INPUT_FASTQ} ${SCRATCH}

ls -la ${SCRATCH}
echo


for f in ${INPUT_FASTQ_IN_SCRATCH}
do
	SUFFIX=${f##*${SAMPLE}.}

	zcat $f |
		awk 'BEGIN{FS="[ \t]+"; line=0}{line=line+1; if (line % 4 == 1){split($0,a); print a[1]}else{print $0}}' |
		gzip -c > ${SCRATCH}/${SAMPLE}.headers_cleaned.${SUFFIX}

done






ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm    ${INPUT_FASTQ_IN_SCRATCH}

mv ${SCRATCH}/${SAMPLE}.headers_cleaned* ${OUT_FOLDER}


ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
     
