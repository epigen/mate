#!/bin/bash

set -e 

PARAMS_FILE="$1"
SAMPLE="$2"
REPORTS="$3"
REPORT_BIND_FOLDER="$4"
STRING="$5"
KRONA_OPTIONS="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tREPORTS=\"${REPORTS}\""
echo -e "\tREPORT_BIND_FOLDER=\"${REPORT_BIND_FOLDER}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tKRONA_OPTIONS=\"${KRONA_OPTIONS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/krona_${STRING}_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/krona_${STRING}_${SAMPLE}_WD"
mkdir -p "${WDIR}"

REPORTS_FILENAMES=""

for R in ${REPORTS}
do
	REPORTS_FILENAMES="${REPORTS_FILENAMES} $(basename ${R})"
done

REPORTS_IN_CONTAINER=""

for R in ${REPORTS_FILENAMES}
do
	REPORTS_IN_CONTAINER="${REPORTS_IN_CONTAINER} ${FOLDER_IN_CONTAINER}/${R}"
done

KRONA_TAXONOMY_FILENAME=$(basename ${KRONA_TAXONOMY_FILE})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tREPORTS_FILENAMES=\"${REPORTS_FILENAMES}\""
echo -e "\tREPORTS_IN_CONTAINER=\"${REPORTS_IN_CONTAINER}\""
echo -e "\tKRONA_TAXONOMY_FILENAME=\"${KRONA_TAXONOMY_FILENAME}\""
echo




if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

REPORTS_IN_SCRATCH=""

for R in ${REPORTS_FILENAMES}
do
	REPORTS_IN_SCRATCH="${REPORTS_IN_SCRATCH} ${SCRATCH}/${R}"
done

echo -e "\tREPORTS_IN_SCRATCH=\"${REPORTS_IN_SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job
cp ${REPORTS}             ${SCRATCH}
cp ${KRONA_TAXONOMY_FILE} ${SCRATCH}

ls -la ${SCRATCH}
echo




# run krona
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_KRONA} \
		ktImportTaxonomy \
			${KRONA_OPTIONS} \
			-tax ${FOLDER_IN_CONTAINER} \
			-o ${FOLDER_IN_CONTAINER}/krona_${STRING}_${SAMPLE}.html \
			${REPORTS_IN_CONTAINER} \
			> ${LOG_SINGULARITY} \
			2>&1



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm ${REPORTS_IN_SCRATCH}
rm ${SCRATCH}/${KRONA_TAXONOMY_FILENAME}

mv ${SCRATCH}/krona_${STRING}_${SAMPLE}.html ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
