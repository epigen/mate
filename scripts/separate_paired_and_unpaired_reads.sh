#!/bin/bash

set -e

PARAMS_FILE="$1"
SAMPLE="$2"
MATE1="$3"
MATE2="$4"
THREADS="$5"
OUT_PAIRED_FOLDER="$6"
OUT_UNPAIRED_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tMATE1=\"${MATE1}\""
echo -e "\tMATE2=\"${MATE2}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tOUT_PAIRED_FOLDER=\"${OUT_PAIRED_FOLDER}\""
echo -e "\tOUT_UNPAIRED_FOLDER=\"${OUT_UNPAIRED_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/separate_paired_and_unpaired_reads_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/separate_paired_and_unpaired_reads_${SAMPLE}_WD"
mkdir -p "${WDIR}"

OUT_FORWARD_PAIRED_READS="${SAMPLE}.paired.mate1.fastq.gz"
OUT_FORWARD_UNPAIRED_READS="${SAMPLE}.unpaired.mate1.fastq.gz"
OUT_REVERSE_PAIRED_READS="${SAMPLE}.paired.mate2.fastq.gz"
OUT_REVERSE_UNPAIRED_READS="${SAMPLE}.unpaired.mate2.fastq.gz"

MATE1_FILENAME=$(basename ${MATE1})
MATE2_FILENAME=$(basename ${MATE2})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tOUT_FORWARD_PAIRED_READS=\"${OUT_FORWARD_PAIRED_READS}\""
echo -e "\tOUT_FORWARD_UNPAIRED_READS=\"${OUT_FORWARD_UNPAIRED_READS}\""
echo -e "\tOUT_REVERSE_PAIRED_READS=\"${OUT_REVERSE_PAIRED_READS}\""
echo -e "\tOUT_REVERSE_UNPAIRED_READS=\"${OUT_REVERSE_UNPAIRED_READS}\""
echo -e "\tMATE1_FILENAME=\"${MATE1_FILENAME}\""
echo -e "\tMATE2_FILENAME=\"${MATE2_FILENAME}\""
echo



if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp ${MATE1} ${SCRATCH}
cp ${MATE2} ${SCRATCH}

ls -la ${SCRATCH}
echo



# run trimmomatic
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_TRIMMOMATIC} \
		trimmomatic PE \
			-threads ${THREADS} -phred33 \
			-trimlog ${FOLDER_IN_CONTAINER}/${SAMPLE}_trimmomatic.trimLog \
			-summary ${FOLDER_IN_CONTAINER}/${SAMPLE}_trimmomatic.sumaryStats \
			${FOLDER_IN_CONTAINER}/${MATE1_FILENAME} \
			${FOLDER_IN_CONTAINER}/${MATE2_FILENAME} \
			${FOLDER_IN_CONTAINER}/${OUT_FORWARD_PAIRED_READS} \
			${FOLDER_IN_CONTAINER}/${OUT_FORWARD_UNPAIRED_READS} \
			${FOLDER_IN_CONTAINER}/${OUT_REVERSE_PAIRED_READS} \
			${FOLDER_IN_CONTAINER}/${OUT_REVERSE_UNPAIRED_READS} \
			MINLEN:1 \
			> ${LOG_SINGULARITY} \
			2>&1

gzip -c ${SCRATCH}/${SAMPLE}_trimmomatic.trimLog > ${SCRATCH}/${SAMPLE}_trimmomatic.trimLog.gz
rm ${SCRATCH}/${SAMPLE}_trimmomatic.trimLog



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm ${SCRATCH}/${MATE1_FILENAME}
rm ${SCRATCH}/${MATE2_FILENAME}

mv ${SCRATCH}/${SAMPLE}_trimmomatic.trimLog.gz  ${LOG_FOLDER}
mv ${SCRATCH}/${SAMPLE}_trimmomatic.sumaryStats ${LOG_FOLDER}

mv ${SCRATCH}/*.unpaired.* ${OUT_UNPAIRED_FOLDER}
mv ${SCRATCH}/*.paired.*   ${OUT_PAIRED_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
