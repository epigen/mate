#!/bin/bash

set -e 

PARAMS_FILE="$1"
INPUT_FILES="$2"
STRING="$3"
OUT_FOLDER="$4"
LOG_FOLDER="$5"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tINPUT_FILES=\"${INPUT_FILES}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/merge_abundances_${STRING}_singularity.log"
WDIR="${LOG_FOLDER}/merge_abundances_${STRING}_WD"
mkdir -p "${WDIR}"



INPUT_FILES_FILENAMES=""

for i in ${INPUT_FILES}
do
	INPUT_FILES_FILENAMES="${INPUT_FILES_FILENAMES} $(basename ${i})"
done


INPUT_FILES_IN_CONTAINER=""

for i in ${INPUT_FILES_FILENAMES}
do
	INPUT_FILES_IN_CONTAINER="${INPUT_FILES_IN_CONTAINER} ${FOLDER_IN_CONTAINER}/${i}"
done

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tINPUT_FILES_FILENAMES=\"${INPUT_FILES_FILENAMES}\""
echo -e "\tINPUT_FILES_IN_CONTAINER=\"${INPUT_FILES_IN_CONTAINER}\""


if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

INPUT_FILES_IN_SCRATCH=""

for i in ${INPUT_FILES_FILENAMES}
do
	INPUT_FILES_IN_SCRATCH="${INPUT_FILES_IN_SCRATCH} ${SCRATCH}/${i}"
done

echo -e "\tINPUT_FILES_IN_SCRATCH=\"${INPUT_FILES_IN_SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job

cp ${INPUT_FILES} ${SCRATCH}

ls -la ${SCRATCH}
echo


echo "print merge util script help:"
echo
# print out metaphlan help
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		merge_metaphlan_tables.py -h \
		>  ${LOG_SINGULARITY} \
		2> ${LOG_SINGULARITY}


echo "run metaphlan4 analysis:"
echo
# run metaphlan
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_METAPHLAN} \
		merge_metaphlan_tables.py \
			${INPUT_FILES_IN_CONTAINER} \
			-o ${FOLDER_IN_CONTAINER}/merged_abundances_${STRING}.txt \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm    ${INPUT_FILES_IN_SCRATCH}

mv ${SCRATCH}/merged_abundances_${STRING}.txt ${OUT_FOLDER}


ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
