#!/bin/bash

set -e 

PARAMS_FILE="$1"
FORWARD_READS_GZ="$2"
REVERSE_READS_GZ="$3"
SAMPLE="$4"
OUT_FOLDER="$5"
LOG_FOLDER="$6"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tFORWARD_READS_GZ=\"${FORWARD_READS_GZ}\""
echo -e "\tREVERSE_READS_GZ=\"${REVERSE_READS_GZ}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/merge_lanes_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/merge_lanes_${SAMPLE}_WD"
mkdir -p "${WDIR}"

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo



zcat ${FORWARD_READS_GZ} \
	| gzip -c \
	> ${OUT_FOLDER}/${SAMPLE}.mate1.gz \
	2> ${LOG_SINGULARITY}

chmod a+x ${OUT_FOLDER}/${SAMPLE}.mate1.gz

zcat ${REVERSE_READS_GZ} \
	| gzip -c \
	> ${OUT_FOLDER}/${SAMPLE}.mate2.gz \
	2> ${LOG_SINGULARITY}

chmod a+x ${OUT_FOLDER}/${SAMPLE}.mate2.gz



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi




