#!/bin/bash

set -e

PARAMS_FILE="$1"
BASENAME="$2"
FASTA="$3"
THREADS="$4"
OUT_FOLDER="$5"
LOG_FOLDER="$6"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tBASENAME=\"${BASENAME}\""
echo -e "\tFASTA=\"${FASTA}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/host_genome_indexing_singularity.log"
WDIR="${LOG_FOLDER}/host_genome_indexing_WD"
mkdir -p "${WDIR}"

FASTA_FILENAME=$(basename ${FASTA})

echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tFASTA_FILENAME=\"${FASTA_FILENAME}\""
echo

if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

cp ${FASTA} ${SCRATCH}

ls -la ${SCRATCH}
echo


# run bowtie2 indexing
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_FASTQ_SCREEN} \
		bowtie2-build \
			--threads ${THREADS} \
			${FOLDER_IN_CONTAINER}/${FASTA_FILENAME} \
			${FOLDER_IN_CONTAINER}/${BASENAME} \
			> ${LOG_SINGULARITY} \
			2>&1



ls -la ${SCRATCH}
echo

# move data out of SCRATCH
rm ${SCRATCH}/${FASTA_FILENAME}
mv ${SCRATCH}/${BASENAME}* ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
