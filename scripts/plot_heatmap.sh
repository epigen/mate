#!/bin/bash

set -e 

PARAMS_FILE="$1"
ABUNDANCES="$2"
STRING="$3"
OUT_FOLDER="$4"
LOG_FOLDER="$5"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tABUNDANCES=\"${ABUNDANCES}\""
echo -e "\tSTRING=\"${STRING}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/plot_heatmap_${STRING}_singularity.log"
WDIR="${LOG_FOLDER}/plot_heatmap_${STRING}_WD"
mkdir -p "${WDIR}"


ABUNDANCES_FILENAME=$(basename ${ABUNDANCES})


echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo -e "\tABUNDANCES_FILENAME=\"${ABUNDANCES_FILENAME}\""
echo


if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo


# copy to scratch all the data that are needed for the job

cp    ${ABUNDANCES} ${SCRATCH}

cat ${SCRATCH}/${ABUNDANCES_FILENAME} \
	| grep -E "s__|clade" \
	| grep -v "t__" \
	| sed "s/^.*|//g" \
	| sed "s/clade_name/sample/g" \
	> ${SCRATCH}/species_only_${ABUNDANCES_FILENAME}

ls -la ${SCRATCH}
echo

echo "head -25 of ${SCRATCH}/species_only_${ABUNDANCES_FILENAME} is:"
head -25 ${SCRATCH}/species_only_${ABUNDANCES_FILENAME}
echo

echo "print hclust2 script help:"
echo
# print out hclust2.py help
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_HCLUST2} \
		hclust2.py -h \
		>  ${LOG_SINGULARITY} \
		2> ${LOG_SINGULARITY}


echo "run hclust2 plotting:"
echo
# run hclust2 plotting
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_HCLUST2} \
		hclust2.py \
			-i ${FOLDER_IN_CONTAINER}/species_only_${ABUNDANCES_FILENAME} \
			-o ${FOLDER_IN_CONTAINER}/heatmap_species_${STRING}.png \
			--legend_file ${FOLDER_IN_CONTAINER}/heatmap_species_${STRING}.legend.png \
			--cell_aspect_ratio 1 \
			--dpi 300 \
			--flabel_size 3 \
			--slabel_size 3 \
			>>  ${LOG_SINGULARITY} \
			2>> ${LOG_SINGULARITY}



ls -la ${SCRATCH}
echo

# distribute data where they should go
# move data out of SCRATCH
rm ${SCRATCH}/${ABUNDANCES_FILENAME}

mv ${SCRATCH}/species_only_${ABUNDANCES_FILENAME}  ${OUT_FOLDER}
mv ${SCRATCH}/heatmap_species_${STRING}.png        ${OUT_FOLDER}


if [ -f "${SCRATCH}/heatmap_species_${STRING}.legend.png" ]
then
	mv ${SCRATCH}/heatmap_species_${STRING}.legend.png ${OUT_FOLDER}
fi

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
