#!/bin/bash

set -e

PARAMS_FILE="$1"
INDEX_BASENAME="$2"
INDEX_FOLDER="$3"
SAMPLE="$4"
INPUT_FILE="$5"
THREADS="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"

echo -e "INPUT PARAMS:"
echo -e "\tPARAMS_FILE=\"${PARAMS_FILE}\""
echo -e "\tINDEX_BASENAME=\"${INDEX_BASENAME}\""
echo -e "\tINDEX_FOLDER=\"${INDEX_FOLDER}\""
echo -e "\tSAMPLE=\"${SAMPLE}\""
echo -e "\tINPUT_FILE=\"${INPUT_FILE}\""
echo -e "\tTHREADS=\"${THREADS}\""
echo -e "\tOUT_FOLDER=\"${OUT_FOLDER}\""
echo -e "\tLOG_FOLDER=\"${LOG_FOLDER}\""
echo



source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/fastq_screen_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/fastq_screen_${SAMPLE}_WD"
mkdir -p "${WDIR}"


echo -e "DERIVED PARAMS:"
echo -e "\tLOG_SINGULARITY=\"${LOG_SINGULARITY}\""
echo -e "\tWDIR=\"${WDIR}\""
echo


if [ "${SCRATCH}" = "" ]
then
	SCRATCH=${WDIR}
fi

echo -e "SCRATCH=\"${SCRATCH}\""
echo

# copy to scratch all the data that are needed for the job
cp    ${INPUT_FILE}   ${SCRATCH}
cp -r ${INDEX_FOLDER} ${SCRATCH}




INPUT_FILE_FILENAME=$(basename ${INPUT_FILE})
LAST_INDEX_FOLDER=$(basename ${INDEX_FOLDER})

# create fastq_screen config file
echo -e "DATABASE\tHost\t${SCRATCH}/${LAST_INDEX_FOLDER}/${INDEX_BASENAME}" > ${SCRATCH}/fastq_screen.conf

ls -la ${SCRATCH}

echo
echo -e "INPUT_FILE_FILENAME=\"${INPUT_FILE_FILENAME}\""
echo -e "LAST_INDEX_FOLDER=\"${LAST_INDEX_FOLDER}\""
echo 

# run fastq screen
singularity exec \
	--cleanenv \
	-B ${SCRATCH}:${FOLDER_IN_CONTAINER} \
	-W ${WDIR} \
	${SIF_PATH_FASTQ_SCREEN} \
		fastq_screen \
			--aligner bowtie2 \
			--conf ${FOLDER_IN_CONTAINER}/fastq_screen.conf \
			--outdir ${FOLDER_IN_CONTAINER} \
			--threads ${THREADS} \
			--nohits \
			/data/${INPUT_FILE_FILENAME} \
			> ${LOG_SINGULARITY} \
			2>&1

ls -la ${SCRATCH}
echo



# distribute data where they should go
# move data out of SCRATCH
rm    ${SCRATCH}/${INPUT_FILE_FILENAME}
rm -r ${SCRATCH}/${LAST_INDEX_FOLDER}
rm    ${SCRATCH}/fastq_screen.conf
mv    ${SCRATCH}/${SAMPLE}* ${OUT_FOLDER}

ls -la ${SCRATCH}



if [ "$(ls -A ${SCRATCH})" ]
then
	echo "Warning: SCRATCH directory ${SCRATCH} is not empty."
	ls -la ${SCRATCH}
fi



if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
	ls -la ${WDIR}
else
	rm -r ${WDIR}
fi
