workdir: config['ANALYSIS_FOLDER']
shell.executable("/bin/bash")
localrules: generate_params_file, check_presence, init, add_fastq_screen_params, remove_host_reads, add_kraken_params, kraken, add_krona_params, krona, add_metaphlan_params, metaphlan

import pandas as pd

file_info = (
	pd.read_csv(config['INPUT_FASTQ_INFO_SHEET'], sep = "\t", header = 0, dtype = "string", comment = '#')
	.set_index(['file_path', 'file_id', 'group'], drop=False)
	.sort_index()
)


rule init:
	input:
		"00_presence/00_presence.done"
	output:
		touch("00_init.done")


rule remove_host_reads:
	input:
		"00_init.done",
		expand("01_cleaned_non_host_reads/merge_lanes_{sample}.done", sample = set(file_info['sample_name']))
	output:
		touch("01_remove_host_reads.done")


		#expand("01_cleaned_non_host_reads/clean_headers_of_paired_reads_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ]),



rule kraken:
	input:
		"01_remove_host_reads.done",
		expand("02_kraken2/bracken_lanes_separately_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ]),
		expand("02_kraken2/bracken_lanes_merged_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ])
	output:
		touch("02_kraken2.done")


rule krona:
	input:
		"02_kraken2.done",
		#expand("03_krona/krona_k2_lanes_separately_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ]),
		expand("03_krona/krona_k2_lanes_together_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ]),
		expand("03_krona/krona_k2_lanes_merged_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ]),
		expand("03_krona/krona_k2_groups_together_{group}.done", group = set(file_info['group'])),
		#expand("03_krona/krona_b_lanes_separately_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ]),
		expand("03_krona/krona_b_lanes_together_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ]),
		expand("03_krona/krona_b_lanes_merged_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ]),
		expand("03_krona/krona_b_groups_together_{group}.done", group = set(file_info['group']))
	output:
		touch("03_krona.done")


rule metaphlan:
	input:
		"03_krona.done",
#		expand("04_metaphlan/metaphlan_lanes_separately_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ] ),
#		expand("04_metaphlan/metaphlan_lanes_merged_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ]),
		"04_metaphlan/plot_heatmap_lanes_separately.done",
		"04_metaphlan/plot_heatmap_lanes_merged.done"
		
	output:
		touch("04_metaphlan.done")


#
# Rules called by the init high-level rule:
#

rule generate_params_file:
	output:
		touch("00_params.done")
	shell:
		'''
		mkdir -p {config[ANALYSIS_FOLDER]}; \
		echo 'PIPELINE_FOLDER=\"'{config[PIPELINE_FOLDER]}'\"' > params.sh; \
		echo 'SCRIPTS_FOLDER=\"'{config[PIPELINE_FOLDER]}'/scripts\"' >> params.sh; \
		echo 'FOLDER_IN_CONTAINER=\"'{config[FOLDER_IN_CONTAINER]}'\"' >> params.sh
		'''

rule check_presence:
	input:
		"00_params.done",
		expand( "{file}", file=list(file_info.file_path) )
	output:
		touch( expand("00_presence/01_files/{file_id}.present", file_id=file_info.file_id) ),
		touch( expand("00_presence/02_sample_lanes/{sample1}_{lane}.present", zip, sample1=file_info.loc[file_info['read']=='mate1', 'sample_name'], lane=file_info.loc[file_info['read']=='mate1', 'lane']) ),
		touch( expand("00_presence/03_samples/{sample}.present", sample=set(file_info.sample_name)) ),
		touch("00_presence/00_presence.done")
	shell:
		'''
		mkdir -p 00_presence/01_files; \
		mkdir -p 00_presence/02_sample_lanes; \
		mkdir -p 00_presence/03_samples
		'''


#
#  Rules called by the remove human reads high-level rule:
#

rule add_fastq_screen_params:
	input:
		"00_params.done"
	output:
		touch("01_params_remove_host_reads.done")
	shell:
		'''
		mkdir -p 01_cleaned_non_host_reads/host_genome_index; \
		mkdir -p 01_cleaned_non_host_reads/fastq_screen; \
		mkdir -p 01_cleaned_non_host_reads/cleaned_fastq/unpaired; \
		mkdir -p 01_cleaned_non_host_reads/lanes_merged_fastq; \
		mkdir -p logs/01_cleaned_non_host_reads; \
		mkdir -p benchmarks/01_cleaned_non_host_reads; \
		echo 'SIF_PATH_FASTQ_SCREEN=\"'{config[SIF_PATH_FASTQ_SCREEN]}'\"' >> params.sh; \
		echo 'SIF_PATH_TRIMMOMATIC=\"'{config[SIF_PATH_TRIMMOMATIC]}'\"' >> params.sh; \
		echo 'HOST_GENOME_REF_FASTA=\"'{config[HOST_GENOME_REF_FASTA]}'\"' >> params.sh
		'''

rule host_genome_indexing:
	input:
		fastqScreenParamsAdded = "01_params_remove_host_reads.done",
		hostGenomeRefFasta = config["HOST_GENOME_REF_FASTA"]
	output:
		touch("01_cleaned_non_host_reads/host_genome_indexing.done")
	log:
		"logs/01_cleaned_non_host_reads/host_genome_indexing.log"
	benchmark:
		"benchmarks/01_cleaned_non_host_reads/host_genome_indexing.tsv"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "01_cleaned_non_host_reads/host_genome_index",
		LOG_FOLDER  = "logs/01_cleaned_non_host_reads"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/index_reference_bowtie2.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[BASENAME]} \
			{input.hostGenomeRefFasta} \
			{config[THREADS_REMOVE_HOST]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/host_genome_indexing_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/host_genome_indexing_snakemake.err
		'''

rule fastq_screen:
	input:
		doneGenomeIndexing = "01_cleaned_non_host_reads/host_genome_indexing.done",
		presenceCheck = lambda wildcards: "00_presence/01_files/"+wildcards.file+".present",
		filePath = lambda wildcards: file_info.loc[ file_info['file_path'].str.contains(wildcards.file), 'file_path']
	output:
		touch("01_cleaned_non_host_reads/fastq_screen_{file}.done")
	log:
		"logs/01_cleaned_non_host_reads/fastq_screen_{file}.log"
	benchmark:
		"benchmarks/01_cleaned_non_host_reads/fastq_screen_{file}.tsv"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "01_cleaned_non_host_reads/fastq_screen",
		LOG_FOLDER  = "logs/01_cleaned_non_host_reads",
		INDEX_FOLDER= "01_cleaned_non_host_reads/host_genome_index"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/fastq_screen.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[BASENAME]} \
			{config[ANALYSIS_FOLDER]}/{params.INDEX_FOLDER} \
			{wildcards.file} \
			{input.filePath} \
			{config[THREADS_REMOVE_HOST]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastq_screen_{wildcards.file}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastq_screen_{wildcards.file}_snakemake.err
		'''

rule separate_paired_and_unpaired_reads:
	input:
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present",
		mate1fastqScreenDone = "01_cleaned_non_host_reads/fastq_screen_{sample_lane}_Unmapped.out.mate1.done",
		mate2fastqScreenDone = "01_cleaned_non_host_reads/fastq_screen_{sample_lane}_Unmapped.out.mate2.done"
	output:
		touch("01_cleaned_non_host_reads/separate_paired_and_unpaired_reads_{sample_lane}.done")
	log:
		"logs/01_cleaned_non_host_reads/separate_paired_and_unpaired_reads_{sample_lane}.log"
	benchmark:
		"benchmarks/01_cleaned_non_host_reads/separate_paired_and_unpaired_reads_{sample_lane}.tsv"
	params:
		PARAMS_FILE         = "params.sh",
		OUT_PAIRED_FOLDER   = "01_cleaned_non_host_reads/cleaned_fastq",
		OUT_UNPAIRED_FOLDER = "01_cleaned_non_host_reads/cleaned_fastq/unpaired",
		LOG_FOLDER          = "logs/01_cleaned_non_host_reads",
		MATE1               = "01_cleaned_non_host_reads/fastq_screen/{sample_lane}_Unmapped.out.mate1.tagged_filter.fastq.gz",
		MATE2               = "01_cleaned_non_host_reads/fastq_screen/{sample_lane}_Unmapped.out.mate2.tagged_filter.fastq.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/separate_paired_and_unpaired_reads.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample_lane} \
			{params.MATE1} \
			{params.MATE2} \
			{config[THREADS_REMOVE_HOST]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_PAIRED_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_UNPAIRED_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/separate_paired_and_unpaired_reads_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/separate_paired_and_unpaired_reads_{wildcards.sample_lane}_snakemake.err
		'''

rule clean_headers_of_paired_reads:
	input:
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present",
		separationDone= "01_cleaned_non_host_reads/separate_paired_and_unpaired_reads_{sample_lane}.done"
	output:
		touch("01_cleaned_non_host_reads/clean_headers_of_paired_reads_{sample_lane}.done")
	log:
		"logs/01_cleaned_non_host_reads/clean_headers_of_paired_reads_{sample_lane}.log"
	benchmark:
		"benchmarks/01_cleaned_non_host_reads/clean_headers_of_paired_reads_{sample_lane}.tsv"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "01_cleaned_non_host_reads/cleaned_fastq",
		LOG_FOLDER  = "logs/01_cleaned_non_host_reads",
		MATE1       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.paired.mate1.fastq.gz",
		MATE2       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.paired.mate2.fastq.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/clean_headers_fastq.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample_lane} \
			"{params.MATE1} {params.MATE2}" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/clean_headers_of_paired_reads_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/clean_headers_of_paired_reads_{wildcards.sample_lane}_snakemake.err
		'''



rule merge_lanes:
	input:
		presenceCheckSamples = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present",
		presenceCheckLanes   = lambda wildcards: expand("00_presence/02_sample_lanes/{{sample}}_{lane}.present", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane']),
		headerCleaningDone   = lambda wildcards: expand("01_cleaned_non_host_reads/clean_headers_of_paired_reads_{{sample}}_{lane}.done", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane'])
	output:
		touch("01_cleaned_non_host_reads/merge_lanes_{sample}.done")
	benchmark:
		"benchmarks/01_cleaned_non_host_reads/merge_lanes_{sample}.tsv"
	log:
		"logs/01_cleaned_non_host_reads/merge_lanes_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "01_cleaned_non_host_reads/lanes_merged_fastq",
		LOG_FOLDER  = "logs/01_cleaned_non_host_reads",
		MATE1_FILES_TO_MERGE = lambda wildcards: expand("01_cleaned_non_host_reads/cleaned_fastq/"+wildcards.sample+"_{lane}.headers_cleaned.paired.mate1.fastq.gz", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane']),
		MATE2_FILES_TO_MERGE = lambda wildcards: expand("01_cleaned_non_host_reads/cleaned_fastq/"+wildcards.sample+"_{lane}.headers_cleaned.paired.mate2.fastq.gz", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane'])
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/merge_lanes.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			\"{params.MATE1_FILES_TO_MERGE}\" \
			\"{params.MATE2_FILES_TO_MERGE}\" \
			{wildcards.sample} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_lanes_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_lanes_{wildcards.sample}_snakemake.err
		'''











#
# Rules called by the kraken high-level rule:
#

rule add_kraken_params:
	input:
		"01_params_remove_host_reads.done"
	output:
		touch("02_params_kraken.done")
	shell:
		'''
		mkdir -p logs/02_kraken2; \
		mkdir -p benchmarks/02_kraken2; \
		mkdir -p 02_kraken2/kraken_lanes_separately; \
		mkdir -p 02_kraken2/kraken_lanes_merged; \
		mkdir -p 02_kraken2/bracken_lanes_separately; \
		mkdir -p 02_kraken2/bracken_lanes_merged; \
		echo 'KRAKEN2_DB=\"'{config[KRAKEN2_DB]}'\"' >> params.sh; \
		echo 'KRAKEN2_ADDITIONAL_OPTIONS=\"'{config[KRAKEN2_ADDITIONAL_OPTIONS]}'\"' >> params.sh; \
		echo 'BRACKEN_ADDITIONAL_OPTIONS=\"'{config[BRACKEN_ADDITIONAL_OPTIONS]}'\"' >> params.sh; \
		echo 'SIF_PATH_KRAKEN2=\"'{config[SIF_PATH_KRAKEN2]}'\"' >> params.sh; \
		echo 'SIF_PATH_BRACKEN=\"'{config[SIF_PATH_BRACKEN]}'\"' >> params.sh
		'''


rule kraken2_lanes_separately:
	input:
		doneParams = "02_params_kraken.done",
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present"
	output:
		touch("02_kraken2/kraken2_lanes_separately_{sample_lane}.done")
	benchmark:
		"benchmarks/02_kraken2/kraken2_lanes_separately_{sample_lane}.tsv"
	log:
		"logs/02_kraken2/kraken2_lanes_separately_{sample_lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "02_kraken2/kraken_lanes_separately",
		LOG_FOLDER  = "logs/02_kraken2",
		MATE1       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.headers_cleaned.paired.mate1.fastq.gz",
		MATE2       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.headers_cleaned.paired.mate2.fastq.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/kraken2.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.MATE1} \
			{config[ANALYSIS_FOLDER]}/{params.MATE2} \
			{wildcards.sample_lane} \
			"lanes_separately" \
			{config[THREADS_KRAKEN2]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/kraken2_lanes_separately_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/kraken2_lanes_separately_{wildcards.sample_lane}_snakemake.err
		'''


rule bracken_lanes_separately:
	input:
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present",
		doneKraken    = "02_kraken2/kraken2_lanes_separately_{sample_lane}.done",
	output:
		touch("02_kraken2/bracken_lanes_separately_{sample_lane}.done")
	benchmark:
		"benchmarks/02_kraken2/bracken_lanes_separately_{sample_lane}.tsv"
	log:
		"logs/02_kraken2/bracken_lanes_separately_{sample_lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_FILE     = "02_kraken2/kraken_lanes_separately/k2_lanes_separately_{sample_lane}_report.txt",
		OUT_FOLDER  = "02_kraken2/bracken_lanes_separately",
		LOG_FOLDER  = "logs/02_kraken2"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/bracken.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_FILE} \
			{wildcards.sample_lane} \
			"lanes_separately" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bracken_lanes_separately_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bracken_lanes_separately_{wildcards.sample_lane}_snakemake.err
		'''




rule kraken2_lanes_merged:
	input:
		doneParams = "02_params_kraken.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("02_kraken2/kraken2_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/02_kraken2/kraken2_lanes_merged_{sample}.tsv"
	log:
		"logs/02_kraken2/kraken2_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "02_kraken2/kraken_lanes_merged",
		LOG_FOLDER  = "logs/02_kraken2",
		MATE1_MERGED= "01_cleaned_non_host_reads/lanes_merged_fastq/{sample}.mate1.gz",
		MATE2_MERGED= "01_cleaned_non_host_reads/lanes_merged_fastq/{sample}.mate2.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/kraken2.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.MATE1_MERGED} \
			{config[ANALYSIS_FOLDER]}/{params.MATE2_MERGED} \
			{wildcards.sample} \
			"lanes_merged" \
			{config[THREADS_KRAKEN2]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/kraken2_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/kraken2_lanes_merged_{wildcards.sample}_snakemake.err
		'''



rule bracken_lanes_merged:
	input:
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present",
		doneKraken    = "02_kraken2/kraken2_lanes_merged_{sample}.done",
	output:
		touch("02_kraken2/bracken_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/02_kraken2/bracken_lanes_merged_{sample}.tsv"
	log:
		"logs/02_kraken2/bracken_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_FILE     = "02_kraken2/kraken_lanes_merged/k2_lanes_merged_{sample}_report.txt",
		OUT_FOLDER  = "02_kraken2/bracken_lanes_merged",
		LOG_FOLDER  = "logs/02_kraken2"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/bracken.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_FILE} \
			{wildcards.sample} \
			"lanes_merged" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bracken_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bracken_lanes_merged_{wildcards.sample}_snakemake.err
		'''






#
# Rules called by the krona high-level rule:
#

rule add_krona_params:
	input:
		"02_params_kraken.done"
	output:
		touch("03_params_krona.done")
	shell:
		'''
		mkdir -p logs/03_krona; \
		mkdir -p benchmarks/03_krona; \
		mkdir -p 03_krona/k2_lanes_together; \
		mkdir -p 03_krona/k2_lanes_merged; \
		mkdir -p 03_krona/k2_groups_together; \
		mkdir -p 03_krona/b_lanes_together; \
		mkdir -p 03_krona/b_lanes_merged; \
		mkdir -p 03_krona/b_groups_together; \
		echo 'KRONA_TAXONOMY_FILE=\"'{config[KRONA_TAXONOMY_FILE]}'\"' >> params.sh; \
		echo 'SIF_PATH_KRONA=\"'{config[SIF_PATH_KRONA]}'\"' >> params.sh
		'''


rule krona_k2_lanes_together:
	input:
		doneParams = "03_params_krona.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("03_krona/krona_k2_lanes_together_{sample}.done")
	benchmark:
		"benchmarks/03_krona/krona_k2_lanes_together_{sample}.tsv"
	log:
		"logs/03_krona/krona_k2_lanes_together_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_REPORTS  = lambda wildcards: expand( config["ANALYSIS_FOLDER"]+"/02_kraken2/kraken_lanes_separately/k2_lanes_separately_"+wildcards.sample+"_{lane}_report.txt ", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane']),
		OUT_FOLDER  = "03_krona/k2_lanes_together",
		LOG_FOLDER  = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "/02_kraken2/kraken_lanes_separately",
		KRONA_OPTIONS         = "-m 3 -t 5"	
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample} \
			\"{params.IN_REPORTS}\" \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"k2_lanes_together" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_lanes_together_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_lanes_together_{wildcards.sample}_snakemake.err
		'''


rule krona_b_lanes_together:
	input:
		doneParams = "03_params_krona.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("03_krona/krona_b_lanes_together_{sample}.done")
	benchmark:
		"benchmarks/03_krona/krona_b_lanes_together_{sample}.tsv"
	log:
		"logs/03_krona/krona_b_lanes_together_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_REPORTS  = lambda wildcards: expand( config["ANALYSIS_FOLDER"]+"/02_kraken2/bracken_lanes_separately/b_lanes_separately_"+wildcards.sample+"_{lane}_output.txt ", lane=file_info.loc[file_info['read']=='mate1'].loc[file_info['sample_name']==wildcards.sample,'lane']),
		OUT_FOLDER  = "03_krona/b_lanes_together",
		LOG_FOLDER  = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "/02_kraken2/bracken_lanes_separately",
		KRONA_OPTIONS         = "-m 6 -t 2"		
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample} \
			\"{params.IN_REPORTS}\" \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"b_lanes_together" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_lanes_together_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_lanes_together_{wildcards.sample}_snakemake.err
		'''





		
rule krona_k2_lanes_merged:
	input:
		doneParams = "03_params_krona.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("03_krona/krona_k2_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/03_krona/krona_k2_lanes_merged_{sample}.tsv"
	log:
		"logs/03_krona/krona_k2_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE           = "params.sh",
		IN_REPORT             = "02_kraken2/kraken_lanes_merged/k2_lanes_merged_{sample}_report.txt", 
		OUT_FOLDER            = "03_krona/k2_lanes_merged",
		LOG_FOLDER            = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "/02_kraken2/lanes_merged",
		KRONA_OPTIONS         = "-m 3 -t 5"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample} \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT} \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"k2_lanes_merged" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_lanes_merged_{wildcards.sample}_snakemake.err
		'''


rule krona_b_lanes_merged:
	input:
		doneParams = "03_params_krona.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("03_krona/krona_b_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/03_krona/krona_b_lanes_merged_{sample}.tsv"
	log:
		"logs/03_krona/krona_b_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE           = "params.sh",
		IN_REPORT             = "02_kraken2/bracken_lanes_merged/b_lanes_merged_{sample}_output.txt", 
		OUT_FOLDER            = "03_krona/b_lanes_merged",
		LOG_FOLDER            = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "/02_kraken2/bracken_lanes_merged",
		KRONA_OPTIONS         = "-m 6 -t 2"	
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample} \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT} \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"b_lanes_merged" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_lanes_merged_{wildcards.sample}_snakemake.err
		'''




rule krona_k2_groups_together:
	input:
		doneParams = "03_params_krona.done",
		GROUP      = lambda wildcards: expand("02_kraken2/kraken2_lanes_merged_{sample}.done", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']))
	output:
		touch("03_krona/krona_k2_groups_together_{group}.done")
	benchmark:
		"benchmarks/03_krona/krona_k2_groups_together_{group}.tsv"
	log:
		"logs/03_krona/krona_k2_groups_together_{group}.log"
	params:
		PARAMS_FILE           = "params.sh",
		IN_REPORTS            = lambda wildcards: expand( config["ANALYSIS_FOLDER"]+"/02_kraken2/kraken_lanes_merged/k2_lanes_merged_{sample}_report.txt ", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']) ),
		OUT_FOLDER            = "03_krona/k2_groups_together",
		LOG_FOLDER            = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "02_kraken2/kraken_lanes_merged",
		KRONA_OPTIONS         = "-m 3 -t 5"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.group} \
			\"{params.IN_REPORTS}\" \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"k2_groups_together" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_groups_together_{wildcards.group}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_k2_groups_together_{wildcards.group}_snakemake.err	
		'''


rule krona_b_groups_together:
	input:
		doneParams = "03_params_krona.done",
		GROUP      = lambda wildcards: expand("02_kraken2/bracken_lanes_merged_{sample}.done", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']))
	output:
		touch("03_krona/krona_b_groups_together_{group}.done")
	benchmark:
		"benchmarks/03_krona/krona_b_groups_together_{group}.tsv"
	log:
		"logs/03_krona/krona_b_groups_together_{group}.log"
	params:
		PARAMS_FILE           = "params.sh",
		IN_REPORTS            = lambda wildcards: expand( config["ANALYSIS_FOLDER"]+"/02_kraken2/bracken_lanes_merged/b_lanes_merged_{sample}_output.txt ", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']) ),
		OUT_FOLDER            = "03_krona/b_groups_together",
		LOG_FOLDER            = "logs/03_krona",
		IN_REPORT_BIND_FOLDER = "02_kraken2/bracken_lanes_merged",
		KRONA_OPTIONS         = "-m 6 -t 2"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/krona.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.group} \
			\"{params.IN_REPORTS}\" \
			{config[ANALYSIS_FOLDER]}/{params.IN_REPORT_BIND_FOLDER} \
			"b_groups_together" \
			\"{params.KRONA_OPTIONS}\" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_groups_together_{wildcards.group}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/krona_b_groups_together_{wildcards.group}_snakemake.err	
		'''



#
# Rules called by the metaphlan high-level rule:
#

rule add_metaphlan_params:
	input:
		"03_params_krona.done"
	output:
		touch("04_params_metaphlan.done")
	shell:
		'''
		mkdir -p logs/04_metaphlan; \
		mkdir -p benchmarks/04_metaphlan; \
		mkdir -p 04_metaphlan/lanes_separately/bowtie2out; \
		mkdir -p 04_metaphlan/lanes_separately/post_mapping; \
		mkdir -p 04_metaphlan/lanes_separately/merged_abundances; \
		mkdir -p 04_metaphlan/lanes_separately/heatmap; \
		mkdir -p 04_metaphlan/lanes_separately/diversities; \
		mkdir -p 04_metaphlan/lanes_merged/bowtie2out; \
		mkdir -p 04_metaphlan/lanes_merged/post_mapping; \
		mkdir -p 04_metaphlan/lanes_merged/merged_abundances; \
		mkdir -p 04_metaphlan/lanes_merged/heatmap; \
		mkdir -p 04_metaphlan/lanes_merged/diversities; \
		echo 'SIF_PATH_METAPHLAN=\"'{config[SIF_PATH_METAPHLAN]}'\"' >> params.sh; \
		echo 'SIF_PATH_HCLUST2=\"'{config[SIF_PATH_HCLUST2]}'\"' >> params.sh; \
		echo 'METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS=\"'{config[METAPHLAN_POST_MAPPING_ADDITIONAL_OPTIONS]}'\"' >> params.sh
		'''


rule metaphlan_lanes_merged:
	input:
		doneParams = "04_params_metaphlan.done",
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present"
	output:
		touch("04_metaphlan/metaphlan_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/04_metaphlan/metaphlan_lanes_merged_{sample}.tsv"
	log:
		"logs/04_metaphlan/metaphlan_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_merged/bowtie2out",
		LOG_FOLDER  = "logs/04_metaphlan",
		MATE1_MERGED= "01_cleaned_non_host_reads/lanes_merged_fastq/{sample}.mate1.gz",
		MATE2_MERGED= "01_cleaned_non_host_reads/lanes_merged_fastq/{sample}.mate2.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/metaphlan.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.MATE1_MERGED} \
			{config[ANALYSIS_FOLDER]}/{params.MATE2_MERGED} \
			{wildcards.sample} \
			"lanes_merged" \
			{config[THREADS_METAPHLAN]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_lanes_merged_{wildcards.sample}_snakemake.err
		'''


rule metaphlan_lanes_separately:
	input:
		doneParams = "04_params_metaphlan.done",
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present"
	output:
		touch("04_metaphlan/metaphlan_lanes_separately_{sample_lane}.done")
	benchmark:
		"benchmarks/04_metaphlan/metaphlan_lanes_separately_{sample_lane}.tsv"
	log:
		"logs/04_metaphlan/metaphlan_lanes_separately_{sample_lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_separately/bowtie2out",
		LOG_FOLDER  = "logs/04_metaphlan",
		MATE1       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.headers_cleaned.paired.mate1.fastq.gz",
		MATE2       = "01_cleaned_non_host_reads/cleaned_fastq/{sample_lane}.headers_cleaned.paired.mate2.fastq.gz"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/metaphlan.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.MATE1} \
			{config[ANALYSIS_FOLDER]}/{params.MATE2} \
			{wildcards.sample_lane} \
			"lanes_separately" \
			{config[THREADS_METAPHLAN]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_lanes_separately_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_lanes_separately_{wildcards.sample_lane}_snakemake.err
		'''


rule metaphlan_post_mapping_lanes_merged:
	input:
		presenceCheck = lambda wildcards: "00_presence/03_samples/"+wildcards.sample+".present",
		metaphlanDone = "04_metaphlan/metaphlan_lanes_merged_{sample}.done"
	output:
		touch("04_metaphlan/metaphlan_post_mapping_lanes_merged_{sample}.done")
	benchmark:
		"benchmarks/04_metaphlan/metaphlan_post_mapping_lanes_merged_{sample}.tsv"
	log:
		"logs/04_metaphlan/metaphlan_post_mapping_lanes_merged_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_merged/post_mapping",
		LOG_FOLDER  = "logs/04_metaphlan",
		BOWTIE2OUT_MERGED= "04_metaphlan/lanes_merged/bowtie2out/mpa4_metagenome_lanes_merged_{sample}.bowtie2.bz2"
	shell:
		'''
		nreads4=`zcat {config[ANALYSIS_FOLDER]}"/01_cleaned_non_host_reads/lanes_merged_fastq/{wildcards.sample}.mate1.gz" | wc -l`; \
		bash {config[PIPELINE_FOLDER]}/scripts/metaphlan_post_mapping.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.BOWTIE2OUT_MERGED} \
			{wildcards.sample} \
			"lanes_merged" \
			{config[THREADS_METAPHLAN]} \
			$((nreads4/2)) \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_post_mapping_lanes_merged_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_post_mapping_lanes_merged_{wildcards.sample}_snakemake.err
		'''

rule metaphlan_post_mapping_lanes_separately:
	input:
		presenceCheck = lambda wildcards: "00_presence/02_sample_lanes/"+wildcards.sample_lane+".present",
		metaphlanDone = "04_metaphlan/metaphlan_lanes_separately_{sample_lane}.done"
	output:
		touch("04_metaphlan/metaphlan_post_mapping_lanes_separately_{sample_lane}.done")
	benchmark:
		"benchmarks/04_metaphlan/metaphlan_post_mapping_lanes_separately_{sample_lane}.tsv"
	log:
		"logs/04_metaphlan/metaphlan_post_mapping_lanes_separately_{sample_lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_separately/post_mapping",
		LOG_FOLDER  = "logs/04_metaphlan",
		BOWTIE2OUT_SEPARATELY = "04_metaphlan/lanes_separately/bowtie2out/mpa4_metagenome_lanes_separately_{sample_lane}.bowtie2.bz2"
	shell:
		'''
		nreads4=`zcat {config[ANALYSIS_FOLDER]}"/01_cleaned_non_host_reads/cleaned_fastq/{wildcards.sample_lane}.headers_cleaned.paired.mate1.fastq.gz" | wc -l`; \
		bash {config[PIPELINE_FOLDER]}/scripts/metaphlan_post_mapping.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.BOWTIE2OUT_SEPARATELY} \
			{wildcards.sample_lane} \
			"lanes_separately" \
			{config[THREADS_METAPHLAN]} \
			$((nreads4/2)) \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_post_mapping_lanes_separately_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/metaphlan_post_mapping_lanes_separately_{wildcards.sample_lane}_snakemake.err
		'''




rule merge_abundances_lanes_separately:
	input:
		doneMpaLanesSeparately = expand("04_metaphlan/metaphlan_post_mapping_lanes_separately_{sample}_{lane}.done", zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ] ) 
	output:
		touch("04_metaphlan/merge_abundances_lanes_separately.done")
	benchmark:
		"benchmarks/04_metaphlan/merge_abundances_lanes_separately.tsv"
	log:
		"logs/04_metaphlan/merge_abundances_lanes_separately.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_separately/merged_abundances",
		LOG_FOLDER  = "logs/04_metaphlan",
		INPUT_FILES = expand(config["ANALYSIS_FOLDER"]+"/04_metaphlan/lanes_separately/post_mapping/mpa4_metagenome_lanes_separately_{sample}_{lane}_profile.txt", 
					zip, sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ], lane = file_info.loc[ file_info['read'] == 'mate1', 'lane' ] )
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/merge_abundances.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			\"{params.INPUT_FILES}\" \
			"lanes_separately" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_abundances_lanes_separately_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_abundances_lanes_separately_snakemake.err
		'''

rule merge_abundances_lanes_merged:
	input:
		doneMpaLanesMerged = expand("04_metaphlan/metaphlan_post_mapping_lanes_merged_{sample}.done", sample = file_info.loc[ file_info['read'] == 'mate1', 'sample_name' ] )
	output:
		touch("04_metaphlan/merge_abundances_lanes_merged.done")
	benchmark:
		"benchmarks/04_metaphlan/merge_abundances_lanes_merged.tsv"
	log:
		"logs/04_metaphlan/merge_abundances_lanes_merged.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_merged/merged_abundances",
		LOG_FOLDER  = "logs/04_metaphlan",
		INPUT_FILES = expand(config["ANALYSIS_FOLDER"]+"/04_metaphlan/lanes_merged/post_mapping/mpa4_metagenome_lanes_merged_{sample}_profile.txt", 
					sample = set(file_info[ 'sample_name' ]) )
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/merge_abundances.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			\"{params.INPUT_FILES}\" \
			"lanes_merged" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_abundances_lanes_merged_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/merge_abundances_lanes_merged_snakemake.err
		'''


rule plot_heatmap_lanes_separately:
	input:
		doneAbundanceMerging = "04_metaphlan/merge_abundances_lanes_separately.done"
	output:
		touch("04_metaphlan/plot_heatmap_lanes_separately.done")
	log:
		"logs/04_metaphlan/plot_heatmap_lanes_separately.log"
	benchmark:
		"benchmarks/04_metaphlan/plot_heatmap_lanes_separately.tsv"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_separately/heatmap",
		LOG_FOLDER  = "logs/04_metaphlan",
		ABUNDANCES  = "04_metaphlan/lanes_separately/merged_abundances/merged_abundances_lanes_separately.txt"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/plot_heatmap.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.ABUNDANCES} \
			"lanes_separately" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/plot_heatmap_lanes_separately_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/plot_heatmap_lanes_separately_snakemake.err
		'''
		


rule plot_heatmap_lanes_merged:
	input:
		doneAbundanceMerging = "04_metaphlan/merge_abundances_lanes_merged.done"
	output:
		touch("04_metaphlan/plot_heatmap_lanes_merged.done")
	log:
		"logs/04_metaphlan/plot_heatmap_lanes_merged.log"
	benchmark:
		"benchmarks/04_metaphlan/plot_heatmap_lanes_merged.tsv"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER  = "04_metaphlan/lanes_merged/heatmap",
		LOG_FOLDER  = "logs/04_metaphlan",
		ABUNDANCES  = "04_metaphlan/lanes_merged/merged_abundances/merged_abundances_lanes_merged.txt"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/plot_heatmap.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.ABUNDANCES} \
			"lanes_merged" \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		>  {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/plot_heatmap_lanes_merged_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/plot_heatmap_lanes_merged_snakemake.err
		'''





